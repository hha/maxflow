#ifndef __dijkstra__
#define __dijkstra__

typedef struct {
  NUMERIC val;
  int idx;
} min_vertex_in_row;

typedef struct {
  int node_idx;
  NUMERIC w_to_node;
  int pre_node;
  int gain;
} path_node;

// Compute for single source
path_node* dijkstra (matrix, int, int, int);

char less (NUMERIC, NUMERIC);

#endif
