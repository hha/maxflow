#include <stdio.h>
#include <stdlib.h>
#include "basic_types.h"
#include "basic_func.h"
#include "dijkstra.h"

int main(int argc, char *argv[]) {
	matrix A_matrix, L_matrix;
  path_node *tracker;
	
	int n = 10;
	
	ALLOC_MATRIX (n, A_matrix, edgeElement)
	
  for (int i = 0; i < n; i++)
    for (int j = 0; j < n; j++)
      { A_matrix[i][j].c = HINFINITY;
				A_matrix[i][j].u = 1;}

  A_matrix[0][1].c = 0;
  A_matrix[0][2].c = 0;
  A_matrix[0][3].c = 0;
  A_matrix[0][4].c = 0;

  A_matrix[5][9].c = 0;
  A_matrix[6][9].c = 0;
  A_matrix[7][9].c = 0;
  A_matrix[8][9].c = 0;

  A_matrix[1][5].c = 1;
  A_matrix[1][6].c = 2;
  A_matrix[1][7].c = 3;
  A_matrix[1][8].c = 4;

  A_matrix[2][5].c = 1;
  A_matrix[2][6].c = 2;
  A_matrix[2][7].c = 3;
  A_matrix[2][8].c = 4;

  A_matrix[3][5].c = 1;
  A_matrix[3][6].c = 2;
  A_matrix[3][7].c = 3;
  A_matrix[3][8].c = 4;

  A_matrix[4][5].c = 1;
  A_matrix[4][6].c = 2;
  A_matrix[4][7].c = 3;
  A_matrix[4][8].c = 4;

  
	print_matrix (A_matrix, n, get_value);
	printf ("\n");
  
	ALLOC_MATRIX (n, L_matrix, edgeElement)
	clone_matrix(n, L_matrix, A_matrix);
	printf("Done cloning\n");
	print_matrix (L_matrix, n, get_value);
	
	tracker = dijkstra (L_matrix, n, 0, 9);
  print_vector (tracker, n, get_tracked_value);
	return 0;
}
