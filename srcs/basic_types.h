#ifndef __basic_types__
#define __basic_types__

#define OUT_OF_RES(msg) \
	{\
		printf("Out of resource for %s \n", (msg));\
		exit(-1);\
	}\

typedef int NUMERIC;

static const NUMERIC HINFINITY = -1;

typedef struct {
	int ID;
	int b; // Suply/demain
	int p; // Potential
} vertexElement;

typedef struct {
  int f; // current flow
	int u; // capacity
	NUMERIC c; // cost per flow
} edgeElement;

typedef edgeElement **matrix;


#endif
