#ifndef __TEST__H__
#define __TEST__H__

/* Test structure */
typedef struct test_s {
    int priority;
} Test;

Test *test_new(int priority);

void test_delete(Test *t);

#endif
