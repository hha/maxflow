#include <stdio.h>
#include <stdlib.h>
#include "basic_types.h"
#include "basic_func.h"
#include "maxflow.h"
#include "resolver.h"

int **resolver (int **input_matrix, int N) { // {{{
	matrix G_matrix, F_matrix;
	int **output_matrix;
	
	int n = (N << 1) + 2; // adding 2 for 2 pseudo nodes (source and sink)
	
	ALLOC_MATRIX (n, G_matrix, edgeElement)
	parse_biparted_2_graph (input_matrix, G_matrix, n);

	ALLOC_MATRIX (n, F_matrix, edgeElement);
	maxflow_mincut (G_matrix, F_matrix, n);
	
	ALLOC_MATRIX (N, output_matrix, int) 
	parse_graph_2_biparted (F_matrix, output_matrix, n);

	return output_matrix;
} // }}}

void parse_biparted_2_graph (int **input_matrix, matrix A, int N) { // {{{
	edgeElement node;
	int simple_size = (N - 2) >> 1;

	node.f = 0;
	node.u = 1;

	for (int i = 0; i < N; i++)
		for (int j = i; j < N; j++) {
			if ( ((i > 0) && (i <=  simple_size))
				&& ((j > simple_size) && (j < N - 1))) {
				node.u = 1;
				node.c = input_matrix[i-1][j-(simple_size+1)];
			} else if ( ((i == 0) && (j >= 1) && (j <= simple_size))
								||((j == N - 1) && (i > simple_size) && (i < N - 1)) ) {
				node.u = 1;
				node.c = 0;
			} else {
				node.u = 0;
				node.c = HINFINITY;
			} 
			A[i][j] = node;
			A[j][i] = A[i][j];
			A[j][i].u = 0;
		}
} // }}}

void parse_graph_2_biparted (matrix A, int **output_matrix, int N) { // {{{
	edgeElement node;
	int simple_size = (N - 2) >> 1;

	node.f = 0;
	node.u = 1;

	for (int i = 0; i < N; i++)
		for (int j = 0; j < N; j++) {
			if ( ((i >= 1) && (i <=  simple_size))
				&& ((j > simple_size) && (j < N - 1))) {
				if (A[i][j].u == 0) output_matrix [i - 1][j - (simple_size + 1)] = 1;
				else output_matrix [i - 1][j - (simple_size + 1)] = 0; 
			}
		}
} // }}}
