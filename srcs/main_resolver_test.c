#include <stdio.h>
#include <stdlib.h>
#include "basic_types.h"
#include "basic_func.h"
#include "resolver.h"

int main(int argc, char *argv[]) {
	int **input_matrix, **flow_matrix;
	int n = 4;
	
	ALLOC_MATRIX (n, input_matrix, int)

	input_matrix[0][0] =2;
	input_matrix[0][1] =3;
	input_matrix[0][2] =6;
	input_matrix[0][3] =7;

	input_matrix[1][0] =1;
	input_matrix[1][1] =2;
	input_matrix[1][2] =5;
	input_matrix[1][3] =6;

	input_matrix[2][0] =2;
	input_matrix[2][1] =1;
	input_matrix[2][2] =2;
	input_matrix[2][3] =3;

	input_matrix[3][0] =3;
	input_matrix[3][1] =2;
	input_matrix[3][2] =1;
	input_matrix[3][3] =2;

	printf ("Input matrix:\n");
	for  (int i = 0; i < n; i++) {
		for  (int j = 0; j < n; j++) printf ("%4d", input_matrix[i][j]);
		printf ("\n");
	}
  
	flow_matrix = resolver (input_matrix, n); 
	
	printf ("Solution:\n");
	for  (int i = 0; i < n; i++) {
		for  (int j = 0; j < n; j++) printf ("%4d", flow_matrix[i][j]);
		printf ("\n");
	}
}
