#include <stdio.h>
#include <stdlib.h>
#include "basic_types.h"
#include "basic_graph.h"

int graph_findnodes (graphADT graph, void *src, void *dst, vertexTag **srcNode, vertexTag **dstNode);

void graph_deledges (edgeTag *edges);

int graph_addvertex (graphADT graph, void *data) { // {{{
	vertexTag *Node, *tmpNode;

	tmpNode = graph->vertices;
	Node = tmpNode;
	while (tmpNode != NULL) {
		Node = tmpNode;
		if (tmpNode->NodeData == data) return 0;
		tmpNode = tmpNode->Next;
	}

	tmpNode = malloc(sizeof(vertexTag));
	if (NULL == tmpNode) OUT_OF_RES("Create new vertex in graph");
	tmpNode->NodeData = data;
	tmpNode->visited = 0;
	tmpNode->Edges = NULL;
	tmpNode->Next = NULL;

	if (NULL == Node) graph->vertices = tmpNode;
	else Node->Next = tmpNode;

	return 1;
} // }}}

int graph_addedge (graphADT graph, void *src, void *dst, void *EdgeData) { // {{{
	vertexTag *srcnode, *dstnode;
	edgeTag *tmpEdge, *Edge;
  int i;

  //printf ("srcnode: %x, dstnode: %x\n", srcnode, dstnode);
	i = graph_findnodes(graph, src, dst, &srcnode, &dstnode);
	if (i != 3) {printf ("Can not find out src node or dst node.\n"); return 0;}
	
  //printf ("srcnode: %x, dstnode: %x\n", srcnode, dstnode);
	Edge = srcnode->Edges;
	tmpEdge = Edge;
	while (tmpEdge != NULL) {
		Edge = tmpEdge;
		if (tmpEdge->EdgeData == EdgeData)
			return 0;
		tmpEdge = tmpEdge->Next;
	}

	tmpEdge = malloc(sizeof(edgeTag));
	if (NULL == tmpEdge) OUT_OF_RES("Create new edge in graph");
	tmpEdge->EdgeData = EdgeData;
	tmpEdge->connectsTo = dstnode;
	tmpEdge->Next = NULL;

	if (NULL == Edge) srcnode->Edges = tmpEdge;
	else Edge->Next = tmpEdge;

	return 1;
} // }}}

graphADT graph_new () { // {{{
	graphADT graph = NULL;
	graph = malloc(sizeof(*graph));
	if (NULL == graph) OUT_OF_RES("Creating new graph");
	graph->vertices = NULL;
	//printf ("Address of vertices: %0d\n", graph->vertices);
	return graph;
} // }}}

void graph_delete (graphADT graph) { // {{{
	vertexTag *Node, *tmpNode;

	if (NULL == graph) {
		printf ("Graph is already NULL.\n");
		return;
	}
	Node = graph->vertices;
	if (NULL == Node) {
		printf ("Vertices list is already NULL.\n");
		return;
	}
	while (Node != NULL) {
		free(Node->NodeData);
		graph_deledges(Node->Edges);
		tmpNode = Node->Next;
		free(Node);
		Node = tmpNode;
	}
} // }}}

// Return 0: found 0 nodes
// Return 1: found src node
// Return 2: found dst node
// Return 3: found both 2 nodes
int graph_findnodes (graphADT graph, void *src, void *dst, vertexTag **srcNode, vertexTag **dstNode) { // {{{
	vertexTag *node;
	int src_found = 0, dst_found = 0;
	node = graph->vertices;
	while (node != NULL) {
		if (node->NodeData == src) {
			*srcNode = node; //printf ("srcNode: %x\n", *srcNode);
			src_found = 1;
		}
		if (node->NodeData == dst) {
			*dstNode = node; //printf ("dstNode: %x\n", *dstNode);
			dst_found = 1;
		}
		node = node->Next;
	}
	return ((dst_found << 1) | src_found);
} // }}}

void graph_deledges (edgeTag *edges) { // {{{
	edgeTag *tmpEdge, *Edge;

	Edge = edges;
	while (Edge != NULL) {
		free(Edge->EdgeData);
		tmpEdge = Edge->Next;
		free(Edge);
		Edge = tmpEdge;
	}
} // }}}
