#include <stdio.h>
#include <stdlib.h>
#include "basic_types.h"
#include "basic_func.h"
#include "basic_pqueue.h"
#include "dijkstra.h"

char less (NUMERIC a, NUMERIC b) { // -1 is treated as infinity
	if (a == HINFINITY) return 0;
	if (b == HINFINITY) return 1;
	if (a < b) return 1;
	return 0;
}

NUMERIC _ADD (NUMERIC a, NUMERIC b) {
	if ((a == HINFINITY) || (b == HINFINITY)) return HINFINITY;
	return a + b;
}

// Compute for single source
path_node* dijkstra (matrix Matrix, int N, int src, int dst) { // {{{
  char *closed;                 // If vertex has been evaluated, its value will be 1 
  int tmp;
  path_node *tracker;
  PQueue *pqueue = pqueue_new (pri_compare, N << 1);
  path_node *node_v, *node_u;
  edgeElement edge;
	int edge_cost;

  // Allocate memory
  //printf("test %d = \n", Matrix[src][0]);
  closed  = malloc(N * sizeof(char));
  tracker = malloc(N * sizeof(path_node));
  if (NULL == closed) OUT_OF_RES("Creating closed array");
  if (NULL == tracker) OUT_OF_RES("Creating tracker array"); 

  // Initialize
  for (tmp = 0; tmp < N; tmp++) {
    int a = 0, i, flowable;

    closed [tmp] = 0;
    tracker[tmp].w_to_node = HINFINITY;
    tracker[tmp].pre_node = tmp;
    tracker[tmp].node_idx = tmp;
    for (int j = tmp+1; j < N; j++)
      if (Matrix[tmp][j].c != HINFINITY) {
        flowable = 1;
        for (i = 1; (i < (N-2)>>1); i++) flowable = (Matrix[i][j].u <= 0) ? 0 : flowable; 
        if ((a < Matrix[tmp][j].c) && flowable) a = Matrix[tmp][j].c;
      }
    tracker[tmp].gain = a; printf ("tracker[%d].gain = %d\n", tmp, a);
  }
  node_v = malloc(sizeof(path_node));
  node_v->node_idx = src;
  node_v->w_to_node = 0;
  node_v->pre_node = src;

  pqueue_enqueue (pqueue, node_v);

  //printf ("Done Dij init\n");
  
  while (pqueue->size > 0) {
  //while ((pqueue->size > 0) && (node_v->node_idx != dst)) {
    node_v = (path_node*) pqueue_dequeue (pqueue);
    printf ("Dequeue node %d.\n", node_v->node_idx);
    closed [node_v->node_idx] = 1;

    for (int i = 0; i < N; i++) {
      if (closed [i] == 0) {
        int d_gain, d_cost;

        edge = Matrix[node_v->node_idx][i];
				edge_cost = edge.u > 0 ? edge.c : HINFINITY;

        node_u = &tracker[i];

        d_gain = (node_u->pre_node == node_u->node_idx) || (node_v->gain == 0) || (tracker[node_u->pre_node].gain == 0) || (_ADD(edge_cost, node_v->w_to_node) == HINFINITY)? 0 : node_v->gain - tracker[node_u->pre_node].gain;
        d_cost = ((node_u->w_to_node != HINFINITY) && (_ADD(edge_cost, node_v->w_to_node) != HINFINITY)) ? 
                    _ADD(edge_cost, node_v->w_to_node) - node_u->w_to_node : 0;
        if (((d_gain > 0) && (d_gain > d_cost)) || ( (d_gain == 0) && less ( _ADD ( edge_cost, node_v->w_to_node), node_u->w_to_node) ) ) 
        {
        printf ("d_gain %d, d_cost %d\n", d_gain, d_cost);
            node_u->w_to_node =  _ADD ( edge_cost, node_v->w_to_node);
            node_u->pre_node = node_v->node_idx;

            pqueue_enqueue (pqueue, node_u);
            printf ("Enqueue node %d.\n", node_u->node_idx);
        }
      }
    }
    
  }

  return tracker;
} // }}}
