#ifndef __basic_func__
#define __basic_func__

#include "basic_types.h"
#include "dijkstra.h"


#define ALLOC_MATRIX(size,Matrix,type_e) {\
	Matrix = malloc((size) * sizeof(void*));\
  if (Matrix == NULL) {\
		printf("Out of memory\\n");\
		exit(-1);\
	}\
	for (int i = 0; i < (size); i++) {\
    Matrix [i] = malloc((size) * sizeof(type_e));\
		if (Matrix[i] == NULL) {\
			printf("Out of memory\\n");\
			exit(-1);\
		}\
	}\
};

int get_value (const void *d);

int get_tracked_value (const void *d);

int pri_compare(const void *d1, const void *d2);

void clone_matrix(int, matrix, matrix);

//void clone_vector(int, vector, vector);

void print_vector (path_node*, int, int (*get_value) (const void *d));

//void clone_fmatrix(int, vector, vector);

int read_matrix (FILE*, matrix, int);  // Read matrix to to 2D array

//int read_fmatrix (FILE*, vector, int);  // Read matrix to a flatten vector 

int write_matrix (char* , matrix, int); 

void print_matrix (matrix, int, int (*get_value) (const void *d));

void get_real_size(int, int, int, int*, int*);

void idx2cord (int, int, int*, int*);

int cord2idx (int, int, int);

//void convert_mat2flat (matrix, int, vector, int, int*, int*);

//void convert_flat2mat (vector, int, matrix, int);

#endif
