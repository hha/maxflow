#ifndef __basic_linkedlistedgraph__
#define __basic_linkedlistedgraph__

#include "basic_types.h"

typedef struct vertexTag {
  void *NodeData;
	int visited;
	struct edgeTag *Edges;
  struct vertexTag *Next;
} vertexTag;

typedef struct edgeTag {
	void *EdgeData;
	vertexTag *connectsTo;
	struct edgeTag *Next;
} edgeTag;

typedef struct graphCDT {
	vertexTag *vertices;
} graphT;

typedef graphT *graphADT;

int graph_addvertex (graphADT graph, void *data);

int graph_addedge (graphADT graph, void *src, void *dst, void *EdgeData);

graphADT graph_new ();

void graph_delete (graphADT graph);
#endif
