#include <stdio.h>
#include <stdlib.h>
#include "basic_graph.h"

int main(int argc, char *argv[]) {
		graphADT graph;
		vertexTag *Node, *DstNode;
		edgeTag *Edge;
		vertexElement NodeData;
		edgeElement EdgeData;

		vertexElement *nodeA = (vertexElement*)malloc(sizeof(vertexElement));	
		vertexElement *nodeB = (vertexElement*)malloc(sizeof(vertexElement));	
		vertexElement *nodeC = (vertexElement*)malloc(sizeof(vertexElement));	
		vertexElement *nodeD = (vertexElement*)malloc(sizeof(vertexElement));	
		vertexElement *nodeE = (vertexElement*)malloc(sizeof(vertexElement));	

		edgeElement *edge1a = (edgeElement*)malloc(sizeof(edgeElement));
		edgeElement *edge1b = (edgeElement*)malloc(sizeof(edgeElement));
		edgeElement *edge2a = (edgeElement*)malloc(sizeof(edgeElement));
		edgeElement *edge2b = (edgeElement*)malloc(sizeof(edgeElement));
		edgeElement *edge3a = (edgeElement*)malloc(sizeof(edgeElement));
		edgeElement *edge3b = (edgeElement*)malloc(sizeof(edgeElement));
		edgeElement *edge4a = (edgeElement*)malloc(sizeof(edgeElement));
		edgeElement *edge4b = (edgeElement*)malloc(sizeof(edgeElement));
		edgeElement *edge5a = (edgeElement*)malloc(sizeof(edgeElement));
		edgeElement *edge5b = (edgeElement*)malloc(sizeof(edgeElement));
		
		nodeA->ID = 0;
		nodeB->ID = 1;
		nodeC->ID = 2;
		nodeD->ID = 3;
		nodeE->ID = 4;

		edge1a->c = 1;
		edge1b->c = 2;
		edge2a->c = 3;
		edge2b->c = 4;
		edge3a->c = 5;
		edge3b->c = 6;
		edge4a->c = 7;
		edge4b->c = 8;
		edge5a->c = 9;
		edge5b->c = 10;

		graph = graph_new ();
		(void) graph_addvertex (graph, (void *) nodeA);
    printf ("A at %x\n", graph->vertices);
		(void) graph_addvertex (graph, (void *) nodeB);
    printf ("B at %x\n", graph->vertices->Next);
		(void) graph_addvertex (graph, (void *) nodeC);
    printf ("C at %x\n", graph->vertices->Next->Next);
		(void) graph_addvertex (graph, (void *) nodeD);
    printf ("D at %x\n", graph->vertices->Next->Next->Next);
		(void) graph_addvertex (graph, (void *) nodeE);
    printf ("E at %x\n", graph->vertices->Next->Next->Next->Next);

		(void) graph_addedge (graph, (void *) nodeA, (void *) nodeB, (void *) edge5a); 
		printf ("A\n");
		Node = graph->vertices;
		Edge = Node->Edges;
		while (Edge != NULL) {
			EdgeData = *(edgeElement*)(Edge->EdgeData);	
			DstNode = Edge->connectsTo;
			printf ("L____%0d____\\ %0d\n", EdgeData.c, ((vertexElement*)(DstNode->NodeData))->ID);
			Edge = Edge->Next;
		}

		(void) graph_addedge (graph, (void *) nodeB, (void *) nodeA, (void *) edge5b); 
		printf ("B\n");
		Node = graph->vertices->Next;
		Edge = Node->Edges;
		while (Edge != NULL) {
			EdgeData = *(edgeElement*)(Edge->EdgeData);	
			DstNode = Edge->connectsTo;
			printf ("L____%0d____\\ %0d\n", EdgeData.c, ((vertexElement*)(DstNode->NodeData))->ID);
			Edge = Edge->Next;
		}

		(void) graph_addedge (graph, (void *) nodeA, (void *) nodeC, (void *) edge1a); 
		printf ("A\n");
		Node = graph->vertices;
		Edge = Node->Edges;
		while (Edge != NULL) {
			EdgeData = *(edgeElement*)(Edge->EdgeData);	
			DstNode = Edge->connectsTo;
			printf ("L____%0d____\\ %0d\n", EdgeData.c, ((vertexElement*)(DstNode->NodeData))->ID);
			Edge = Edge->Next;
		}

		(void) graph_addedge (graph, (void *) nodeC, (void *) nodeA, (void *) edge1b); 
		printf ("C\n");
		Node = graph->vertices->Next->Next;
		Edge = Node->Edges;
		while (Edge != NULL) {
			EdgeData = *(edgeElement*)(Edge->EdgeData);	
			DstNode = Edge->connectsTo;
			printf ("L____%0d____\\ %0d\n", EdgeData.c, ((vertexElement*)(DstNode->NodeData))->ID);
			Edge = Edge->Next;
		}

		(void) graph_addedge (graph, (void *) nodeC, (void *) nodeE, (void *) edge2a); 
		printf ("C\n");
		Node = graph->vertices->Next->Next;
		Edge = Node->Edges;
		while (Edge != NULL) {
			EdgeData = *(edgeElement*)(Edge->EdgeData);	
			DstNode = Edge->connectsTo;
			printf ("L____%0d____\\ %0d\n", EdgeData.c, ((vertexElement*)(DstNode->NodeData))->ID);
			Edge = Edge->Next;
		}

		(void) graph_addedge (graph, (void *) nodeE, (void *) nodeC, (void *) edge2b); 
		printf ("E\n");
		Node = graph->vertices->Next->Next->Next->Next;
		Edge = Node->Edges;
		while (Edge != NULL) {
			EdgeData = *(edgeElement*)(Edge->EdgeData);	
			DstNode = Edge->connectsTo;
			printf ("L____%0d____\\ %0d\n", EdgeData.c, ((vertexElement*)(DstNode->NodeData))->ID);
			Edge = Edge->Next;
		}

		(void) graph_addedge (graph, (void *) nodeE, (void *) nodeD, (void *) edge3a); 
		printf ("E\n");
		Node = graph->vertices->Next->Next->Next->Next;
		Edge = Node->Edges;
		while (Edge != NULL) {
			EdgeData = *(edgeElement*)(Edge->EdgeData);	
			DstNode = Edge->connectsTo;
			printf ("L____%0d____\\ %0d\n", EdgeData.c, ((vertexElement*)(DstNode->NodeData))->ID);
			Edge = Edge->Next;
		}

		(void) graph_addedge (graph, (void *) nodeD, (void *) nodeE, (void *) edge3b); 
		printf ("D\n");
		Node = graph->vertices->Next->Next->Next;
		Edge = Node->Edges;
		while (Edge != NULL) {
			EdgeData = *(edgeElement*)(Edge->EdgeData);	
			DstNode = Edge->connectsTo;
			printf ("L____%0d____\\ %0d\n", EdgeData.c, ((vertexElement*)(DstNode->NodeData))->ID);
			Edge = Edge->Next;
		}

		(void) graph_addedge (graph, (void *) nodeD, (void *) nodeB, (void *) edge4a); 
		printf ("D\n");
		Node = graph->vertices->Next->Next->Next;
		Edge = Node->Edges;
		while (Edge != NULL) {
			EdgeData = *(edgeElement*)(Edge->EdgeData);	
			DstNode = Edge->connectsTo;
			printf ("L____%0d____\\ %0d\n", EdgeData.c, ((vertexElement*)(DstNode->NodeData))->ID);
			Edge = Edge->Next;
		}

		(void) graph_addedge (graph, (void *) nodeB, (void *) nodeD, (void *) edge4b); 
		printf ("B\n");
		Node = graph->vertices->Next;
		Edge = Node->Edges;
		while (Edge != NULL) {
			EdgeData = *(edgeElement*)(Edge->EdgeData);	
			DstNode = Edge->connectsTo;
			printf ("L____%0d____\\ %0d\n", EdgeData.c, ((vertexElement*)(DstNode->NodeData))->ID);
			Edge = Edge->Next;
		}

		// Print the graph
    printf("=========================\n");
		Node = graph->vertices;
		while (Node != NULL) {
			NodeData = *(vertexElement*)(Node->NodeData);
			printf ("%0d\n", NodeData.ID);
			Edge = Node->Edges;
			while (Edge != NULL) {
				EdgeData = *(edgeElement*)(Edge->EdgeData);	
				DstNode = Edge->connectsTo;
				printf ("L____%0d____\\ %0d\n", EdgeData.c, ((vertexElement*)(DstNode->NodeData))->ID);
				Edge = Edge->Next;
			}
			Node = Node->Next;
		}

    return (0);
}
