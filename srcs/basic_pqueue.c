#include <stdio.h>
#include <stdlib.h>
#include "basic_types.h"
#include "basic_pqueue.h"

#define LEFT(x) (2 * (x) + 1)
#define RIGHT(x) (2 * (x) + 2)
#define PARENT(x) ((x) / 2)

void pqueue_heapify (PQueue *q, size_t idx);

PQueue *pqueue_new (int (*cmp) (const void *d1, const void *d2), size_t capacity) {
	PQueue *res = NULL;
	res = malloc(sizeof(*res));
	if (res == NULL) OUT_OF_RES("new pqueue");
	res->cmp = cmp;
	res->capacity = capacity;
	res->data = malloc(capacity * sizeof(*(res->data)));
	if (res->data == NULL) OUT_OF_RES("allocate data");
	res->size = 0;
	return (res);
}

void pqueue_delete (PQueue *q) {
	if (NULL == q) {
		printf ("Priority Queue is already NULL.\n");
		return;
	}
	free (q->data);
	free (q);
}

void pqueue_enqueue (PQueue *q, const void *data) {
	size_t i;
	void *tmp = NULL;
	if (q->size >= q->capacity) {
		printf ("Priority Queue is full. Can not add element.\n");
		return;
	}
	q->data[q->size] = (void*) data;
	i = q->size++;
	
	while ((i > 0) && (q->cmp(q->data[i], q->data[PARENT(i)]) < 0)) {
		tmp = q->data[i];
		q->data[i] = q->data[PARENT(i)];
		q->data[PARENT(i)] = tmp;
		i = PARENT(i);
	}
}

void *pqueue_dequeue (PQueue *q) {
	void *data = NULL;
	if (q->size > 0) {
		data = q->data[0];
		q->data[0] = q->data[q->size - 1];
		q->size--;
		pqueue_heapify (q, 0);
		return (data);
	} else return NULL;
}

void pqueue_heapify (PQueue *q, size_t idx) {
/* left index, right index, smallest */
  void *tmp = NULL;
  size_t l_idx, r_idx, sml_idx;

  l_idx = LEFT(idx);
  r_idx = RIGHT(idx);

  /* Left child exists, compare left child with its parent */
  if ((l_idx < q->size) && (q->cmp(q->data[l_idx], q->data[idx]) < 0)) {
      sml_idx = l_idx;
  } else {
      sml_idx = idx;
  }

  /* Right child exists, compare right child with the smallest element */
  if ((r_idx < q->size) && (q->cmp(q->data[r_idx], q->data[sml_idx]) < 0)) {
      sml_idx = r_idx;
  }

  if (sml_idx != idx) {
      /* Swap between the index and the smallest element */
      tmp = q->data[sml_idx];
      q->data[sml_idx] = q->data[idx];
      q->data[idx] = tmp;
      /* Heapify again */
      pqueue_heapify(q, sml_idx);
  }
}
