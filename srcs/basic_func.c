#include <stdio.h>
#include <stdlib.h>
#include "basic_types.h"
#include "basic_func.h"
#include "dijkstra.h"

int pri_compare(const void *d1, const void *d2) {
    int dw;
    dw = (unsigned int)(((path_node*)d1)->w_to_node) - (unsigned int)(((path_node*)d2)->w_to_node);
    return dw;
}

void clone_matrix (int N, matrix D, matrix S) {
	for (int i = 0; i < N; i++)
		for (int j = 0; j < N; j++)
			D[i][j] = S[i][j];
}

//void clone_vector (int N, vector D, vector S) {
//	for (int i = 0; i < N; i++)
//			D[i] = S[i];
//}

int get_value (const void *d) { // {{{
  return ((edgeElement*)d)->c;
} // }}}

int get_tracked_value (const void *d) { // {{{
  return ((path_node*)d)->pre_node;
} // }}}

void print_vector (path_node *D, int N, int (*get_tracked_value) (const void *d)) {
	for (int i = 0; i < N; printf("%d ", get_tracked_value((void *)(&D[i]))), i++);
	printf ("\n");
}

int read_matrix (FILE *fp, matrix A_array, int n) { // {{{
	int x;         // Temporary variable

  //Parse data to matrix
	for (int i = 0; i < n; i++)
		for (int j = 0; j < n; j++) {
			x = fscanf (fp, "%d", &A_array[i][j]);
			if (x == EOF) {
				printf ("Data file is not correctly formatted\n");
				fclose(fp);
				return 1;
			}
		}
		
	fclose(fp);
	return 0;
} // }}}

//int read_fmatrix (FILE *fp, vector A_array, int n) { // {{{
//	int x;         // Temporary variable
//
//  //Parse data to matrix
//	for (int i = 0; i < n; i++)
//		for (int j = 0; j < n; j++) {
//			x = fscanf (fp, "%d", &A_array[i*n + j]);
//			if (x == EOF) {
//				printf ("Data file is not correctly formatted\n");
//				fclose(fp);
//				return 1;
//			}
//		}
//		
//	fclose(fp);
//	return 0;
//} // }}}

int write_matrix (char filename[40], matrix L_array, int n) { // {{{
	FILE *fp;      // file pointer
	int x;
	
	fp = fopen(filename, "w+");
	if (fp == NULL) {
		printf ("File %s could not be opened\n", filename);
		return 1;
	}

  //printf ("%d\n",n);
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) x = fprintf (fp, "%4d", L_array[i][j]);
    x = fprintf (fp,"\n");
	}
	fclose(fp);
	return 0;
} // }}}

void print_matrix (matrix array, int N, int (*get_value) (const void *d) ) { // {{{
	for (int i = 0; i < N; i++) {
		for (int j = 0; j < N; j++) printf ("%4d", get_value((void *)(&array[i][j])));
		printf ("\n");
	}
} // }}}

void get_real_size(int N, int n_e, int rank, int *r_e, int *c_e) {
  int x, cpos, rpos;
  
  x = 1 + ( (N - 1) / n_e);
  cpos = rank % x; rpos = rank / x;
  //printf ("[%d] cpos = %d, rpos = %d, n_e = %d, N = %d\n", rank, cpos, rpos, n_e, N);
  *c_e = (cpos == (x - 1)) ? N - n_e*cpos : n_e;
  *r_e = (rpos == (x - 1)) ? N - n_e*rpos : n_e;
}

void idx2cord (int idx, int N, int *row, int *col) {
	*row = idx / N;
	*col = idx % N;
}

int cord2idx (int row, int col, int N) {
	return (row * N) + col;
}

//void convert_mat2flat (matrix M, int N, vector F, int n_sqrt, int *len, int *disp) {
//	int i, j, k, h, idx;
//	int n_e, r_e, c_e, start_col, start_row;
//	
//	n_e = 1 + ((N - 1) / n_sqrt);
//	idx = 0;
//	
//	for (i = 0; i < n_sqrt; i++) {
//		for (j = 0; j < n_sqrt; j++) {
//			get_real_size(N, n_e, i*n_sqrt + j, &r_e, &c_e);
//			start_row = i*n_e;
//			start_col = j*n_e;
//			len  [i*n_sqrt + j] = r_e * c_e;
//			disp [i*n_sqrt + j] = idx;
//			for (k = 0; k < r_e; k++) {
//				for (h = 0; h < c_e; h++) {
//					F [idx] = M[k+start_row][h+start_col];
//					idx++;
//				}
//			}
//		}
//	}
//}

//void convert_flat2mat (vector F, int n_sqrt, matrix M, int N) {
//	int i, j, k, h, idx;
//	int n_e, r_e, c_e, start_col, start_row;
//	
//	n_e = 1 + ((N - 1) / n_sqrt);
//	//printf("n_e = %d\n", n_e);
//	idx = 0;
//	for (i = 0; i < n_sqrt; i++) {
//		for (j = 0; j < n_sqrt; j++) {
//			get_real_size(N, n_e, i*n_sqrt + j, &r_e, &c_e);
//			//printf("Node %d, r_e = %d, c_e = %d\n", i*n_sqrt + j, r_e, c_e);
//			start_row = i*n_e;
//			start_col = j*n_e;
//			for (k = 0; k < r_e; k++) {
//				for (h = 0; h < c_e; h++) {
//					M[k+start_row][h+start_col] = F[idx];
//					idx++;
//				}
//			}
//		}
//	}
//}
