#ifndef __maxflow__
#define __maxflow__
#include "basic_types.h"

void maxflow_mincut(matrix G_matrix, matrix F_matrix, int N);

NUMERIC min_capacity(path_node *tracker, int src, int dst, matrix G); 

#endif
