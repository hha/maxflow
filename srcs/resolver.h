#ifndef __resolver__
#define __resolver__

#include "basic_types.h"

int **resolver (int **input_matrix, int N);

void parse_biparted_2_graph (int **input_matrix, matrix A, int N);

void parse_graph_2_biparted (matrix A, int **input_matrix, int N);

#endif
