#include <stdio.h>
#include <stdlib.h>
#include "test.h"

Test *test_new(int priority) {
    Test *t = NULL;
    t = malloc(sizeof(*t));
    t->priority = priority;
    return (t);
}

void test_delete(Test *t) {
    if (NULL != t) {
        free(t);
    }
}
