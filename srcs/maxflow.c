#include <stdio.h>
#include <stdlib.h>
#include "basic_types.h"
#include "basic_func.h"
#include "dijkstra.h"
#include "maxflow.h"

void maxflow_mincut(matrix G_matrix, matrix F_matrix, int N) { // {{{
	matrix residual_matrix;
  path_node *tracker;
  int min_u, i, j;

	print_matrix (G_matrix, N, get_value);
	printf ("\n");
  
	ALLOC_MATRIX (N, residual_matrix, edgeElement)
	clone_matrix(N, residual_matrix, G_matrix);
	printf("Done cloning\n");
  for (int r = 0; r < N; r++) {
    for (int c = 0; c < N; c++) printf ("%4d", residual_matrix[r][c].u);
    printf ("\n");
  }
		
	tracker = dijkstra (residual_matrix, N, 0, N-1);
  print_vector (tracker, N, get_tracked_value);
	min_u = min_capacity(tracker, 0, N-1, residual_matrix);

	while (min_u != HINFINITY) {
		i = N - 1;
		while (i > 0) {
			j = tracker[i].pre_node;	
			if (G_matrix[i][j].u > 0) {
				(residual_matrix [i][j]).u = (residual_matrix [i][j]).u - min_u;
				(residual_matrix [j][i]).u = (residual_matrix [j][i]).u + min_u;
			} else {
				(residual_matrix [i][j]).u = (residual_matrix [i][j]).u + min_u;
				(residual_matrix [j][i]).u = (residual_matrix [j][i]).u - min_u;
			}
		i = j;
		}
    for (int r = 0; r < N; r++) {
      for (int c = 0; c < N; c++) printf ("%4d", residual_matrix[r][c].u);
      printf ("\n");
    }
		tracker = dijkstra (residual_matrix, N, 0, N-1);
  	print_vector (tracker, N, get_tracked_value);
		min_u = min_capacity(tracker, 0, N-1, residual_matrix);
	}

	clone_matrix(N, F_matrix, residual_matrix);
} // }}}

NUMERIC min_capacity(path_node *tracker, int src, int dst, matrix G) { // {{{
	NUMERIC min_u = HINFINITY;
	path_node current = tracker[dst];
	
	if (current.w_to_node == HINFINITY) return HINFINITY;

	while (current.node_idx != src) {
		if ( less ( G[current.pre_node][current.node_idx].u, min_u))
			min_u = G[current.pre_node][current.node_idx].u;
		current = tracker[current.pre_node];
	}

	return min_u;
} // }}}
